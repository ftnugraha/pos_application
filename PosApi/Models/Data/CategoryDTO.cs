﻿namespace PosApi.Models.Data
{
    //for update partial (HttpPatch)
    public class CategoryDTO
    {
        public string NameCategory { get; set; }
        public string Description { get; set; }
    }
}
