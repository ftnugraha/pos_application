﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace PosApi.Models;

public partial class TblCategory
{
    public int Id { get; set; }

    [DisplayName("Name Category")]
    public string NameCategory { get; set; } = null!;

    public string? Description { get; set; }

    public bool? IsDelete { get; set; }

    public int CreateBy { get; set; }

    [DisplayName("Created Date")]
    public DateTime CreateDate { get; set; }

    public int? UpdateBy { get; set; }

    public DateTime? UpdateDate { get; set; }
}
