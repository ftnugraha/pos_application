﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Pos_ViewModel;
using PosApi.Models;

namespace PosApi.Controllers
{
    [Route("api/ProductApi")]
    [ApiController]
    public class ProductApiController : ControllerBase
    {
        private readonly PosDbContext db;

        public ProductApiController(PosDbContext _db)
        {
            this.db = _db;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<VM_Product>))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetProducts()
        {
            try
            {
                List<VM_Product> products = (from p in db.TblProducts join v in db.TblVariants on p.IdVariant equals v.Id
                                             join c in db.TblCategories on v.IdCategory equals c.Id
                                             where p.IsDelete == false
                                             select new VM_Product
                                             {
                                                 Id = p.Id,
                                                 NameProduct = p.NameProduct,
                                                 NameCategory = c.NameCategory,
                                                 NameVariant = v.NameVariant,
                                                 Price = p.Price,
                                                 Stock = p.Stock,
                                                 IdVariant = p.IdVariant,
                                                 Image = p.Image,
                                                 IsDelete = p.IsDelete,
                                                 CreateBy = p.CreateBy,
                                                 CreateDate = p.CreateDate,
                                                 UpdateBy = p.UpdateBy, 
                                                 UpdateDate = p.UpdateDate
                                             }).ToList();

                return Ok(products);
            }
            catch (Exception ex)
            {
                // Handle exceptions and return appropriate response
                return StatusCode(StatusCodes.Status500InternalServerError, $"An error occurred: {ex.Message}");
            }
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(TblProduct))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetProductById(int id)
        {
            try
            {
                // Check if id = 0
                if(id == 0)
                {
                    return BadRequest();
                }

                // Find product based on id and isDelete = false
                TblProduct product = db.TblProducts.Where(x => x.Id == id && x.IsDelete == false).FirstOrDefault();

                // Check if variant is null
                if (product == null)
                {
                    return NotFound("Product not found");
                }

                // Return product if found
                return Ok(product);
            }
            catch (Exception ex)
            {
                // Handle exceptions and return appropriate response
                return StatusCode(StatusCodes.Status500InternalServerError, $"An error occurred: {ex.Message}");
            }
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(TblProduct))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult CreateProduct([FromBody] TblProduct product)
        {
            try
            {
                // Validate input if product = null
                if (product == null)
                {
                    return BadRequest("Invalid data product");
                }

                // Check duplicate name product
                if(db.TblProducts.Any(x => x.NameProduct.ToLower() == product.NameProduct.ToLower() && x.IsDelete == false))
                {
                    ModelState.AddModelError("customError", $"Product name : {product.NameProduct} already exist");
                    return BadRequest(ModelState);
                }

                // Initialize additional properties before adding to the database
                product.CreateDate = DateTime.Now;
                product.CreateBy = 1;
                product.IsDelete = false;

                // Add new product to the database
                db.TblProducts.Add(product);
                db.SaveChanges();

                // Return respons HTTP 201 Created
                return CreatedAtAction(nameof(GetProductById), new { id = product.Id }, product);
            }
            catch (Exception ex)
            {
                // Handle exceptions and return appropriate response
                return StatusCode(StatusCodes.Status500InternalServerError, $"An error occurred: {ex.Message}");
            }
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<TblProduct> UpdateProduct(int id, TblProduct data)
        {
            try
            {
                // Check if the updated product data is valid
                if (data == null || id != data.Id)
                {
                    return BadRequest("Invalid product data");
                }

                // Find the existing product in the database
                TblProduct product = db.TblProducts.Find(id);

                // Check if the variant exists
                if (product == null)
                {
                    return NotFound("Product not found");
                }

                // Check duplicate name product
                if (db.TblProducts.Any(x => x.Id != id && x.NameProduct.ToLower() == data.NameProduct.ToLower() && x.IsDelete == false))
                {
                    ModelState.AddModelError("customError", $"Product name : {data.NameProduct} already exist");
                    return BadRequest(ModelState);
                }

                // Update the existing product properties
                product.IdVariant = data.IdVariant;
                product.NameProduct = data.NameProduct;
                product.Price = data.Price;
                product.Stock = data.Stock;
                product.Image = data.Image;
                product.UpdateBy = 1;
                product.UpdateDate = DateTime.Now;

                // Save changes to the database
                db.SaveChanges();

                // Return success message
                return Ok("Product updated successfully");
            }
            catch (Exception ex)
            {
                // Handle exceptions and return appropriate response
                return StatusCode(StatusCodes.Status500InternalServerError, $"An error occurred: {ex.Message}");
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult DeleteProduct(int id)
        {
            try
            {
                // Find the product will be delete
                TblProduct product = db.TblProducts.Find(id);

                if (id == 0)
                {
                    return BadRequest();
                }

                // Check if the product exists
                if (product == null)
                {
                    return NotFound("Product not found");
                }

                // Set value IsDelete to be true
                product.IsDelete = true;

                // Save changes to the database
                db.SaveChanges();

                // Return success message
                return Ok($"Product {product.NameProduct} deleted successfully");
            }
            catch (Exception ex)
            {
                // Handle exceptions and return appropriate response
                return StatusCode(StatusCodes.Status500InternalServerError, $"An error occurred: {ex.Message}");
            }
        }
    }
}
