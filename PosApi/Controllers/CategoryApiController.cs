﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Pos_ViewModel;
using PosApi.Models;
using PosApi.Models.Data;

namespace PosApi.Controllers
{
    [Route("api/CategoryApi")]
    [ApiController]
    public class CategoryApiController : ControllerBase
    {
        private readonly PosDbContext db;

        //constructor
        public CategoryApiController(PosDbContext _db)
        {
            this.db = _db;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<TblCategory>))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetCategories()
        {
            try
            {
                List<TblCategory> categories = db.TblCategories.Where(x => x.IsDelete == false).ToList();

                return Ok(categories);
            }
            catch (Exception ex)
            {
                // Handle exceptions and return appropriate response
                return StatusCode(StatusCodes.Status500InternalServerError, $"An error occurred: {ex.Message}");
                
            }
            
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<TblCategory> GetCategoryById(int id)
        {
            //Check if id = 0
            if(id == 0)
            {
                return BadRequest();
            }

            TblCategory category = db.TblCategories.FirstOrDefault(x => x.Id == id && x.IsDelete == false);

            //Chack if the category is null
            if(category == null)
            {
                return NotFound();
            }

            //Return success
            return Ok(category);
        }

        // ...

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(TblCategory))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult CreateCategory([FromBody] TblCategory category)
        {
            try
            {
                // Check if the category is null
                if (category == null)
                {
                    return BadRequest("Invalid data category");
                }

                // Check for duplicate category name
                if (db.TblCategories.Any(x => x.NameCategory.ToLower() == category.NameCategory.ToLower() && x.IsDelete == false))
                {
                    //ModelState.AddModelError("customError", "Category already exists");
                    return BadRequest(new VM_Response {Success = false, Message = "Category already exists" });
                }

                // Initialize additional properties before adding to the database
                category.CreateDate = DateTime.Now;
                category.CreateBy = 1;
                category.IsDelete = false;

                // Add new category to the database
                db.TblCategories.Add(category);
                db.SaveChanges();

                // Return response HTTP 201 Created
                return CreatedAtAction(nameof(GetCategoryById), new { id = category.Id }, category);
            }
            catch (Exception ex)
            {
                // Handle exceptions and return appropriate response
                return StatusCode(StatusCodes.Status500InternalServerError, $"An error occurred: {ex.Message}");
            }
        }

        // ...


        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<TblCategory> UpdateCategory(int id, TblCategory data)
        {
            try
            {
                // Check if the updated category data is valid
                if (data == null || id != data.Id)
                {
                    return BadRequest("Invalid category data");
                }

                // Find the existing category in the database
                TblCategory category = db.TblCategories.Find(id);

                // Check if the category exists
                if (category == null)
                {
                    return NotFound("Category not found");
                }

                // Check for duplicate category name
                if (db.TblCategories.Any(x => x.Id != id && x.NameCategory.ToLower() == data.NameCategory.ToLower() && x.IsDelete == false))
                {
                    ModelState.AddModelError("customError", "Category name already exists");
                    return BadRequest(ModelState);
                }

                // Update the existing category properties
                category.NameCategory = data.NameCategory;
                category.Description = data.Description;
                category.UpdateBy = 1;
                category.UpdateDate = DateTime.Now;

                // Save changes to the database
                db.SaveChanges();

                // Return success message
                return Ok(new VM_Response { Success = true, Message = "Category updated successfully" }) ;
            }
            catch (Exception ex)
            {
                // Handle exceptions and return appropriate response
                return StatusCode(StatusCodes.Status500InternalServerError, $"An error occurred: {ex.Message}");
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult DeleteCategory(int id)
        {
            try
            {
                // Find the category will be delete
                TblCategory category = db.TblCategories.Find(id);

                if(id == 0)
                {
                    return BadRequest();
                }

                // Check if the category exists
                if (category == null)
                {
                    return NotFound("Category not found");
                }

                // Set value IsDelete to be true
                category.IsDelete = true;

                // Save changes to the database
                db.SaveChanges();

                // Return success message
                return Ok($"Category {category.NameCategory} deleted successfully");
            }
            catch (Exception ex)
            {
                // Handle exceptions and return appropriate response
                return StatusCode(StatusCodes.Status500InternalServerError, $"An error occurred: {ex.Message}");
            }
        }

        [HttpPatch("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<TblCategory> PatchCategory(int categoryId, CategoryDTO categoryUpdateDto)
        {
            try
            {
                // Find the category will be update
                TblCategory categoryToUpdate = db.TblCategories.Find(categoryId);

                // Check if the category exists
                if (categoryToUpdate == null)
                {
                    return NotFound("Category not found");
                }

                // partial update
                if (categoryUpdateDto.NameCategory != null)
                {
                    categoryToUpdate.NameCategory = categoryUpdateDto.NameCategory;
                }

                if (categoryUpdateDto.Description != null)
                {
                    categoryToUpdate.Description = categoryUpdateDto.Description;
                }

                // Save changes to the database
                db.SaveChanges();

                // Return updated category
                return Ok(categoryToUpdate);
            }
            catch (Exception ex)
            {
                // Handle exceptions and return appropriate response
                return StatusCode(StatusCodes.Status500InternalServerError, $"An error occurred: {ex.Message}");
            }
        }

    }
}
