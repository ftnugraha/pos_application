﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PosApi.Models;
using Pos_ViewModel;

namespace PosApi.Controllers
{
    [Route("api/VariantApi")]
    [ApiController]
    public class VariantApiController : ControllerBase
    {
        private readonly PosDbContext db;
        public VariantApiController(PosDbContext _db)
        {
            this.db = _db;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<VM_Variant>))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetVariants()
        {
            try
            {
                //Join TblVarian with TblCategory to show NameCategory
                List<VM_Variant> variants = (from v in db.TblVariants
                                             join c in db.TblCategories
                                             on v.IdCategory equals c.Id
                                             where v.IsDelete == false
                                             select new VM_Variant
                                             {
                                                 Id = v.Id,
                                                 IdCategory = v.IdCategory,
                                                 NameVariant = v.NameVariant,
                                                 NameCategory = c.NameCategory,
                                                 Description = v.Description,
                                                 IsDelete = v.IsDelete,
                                                 CreateBy = v.CreateBy,
                                                 CreateDate = v.CreateDate,
                                                 UpdateBy = v.UpdateBy,
                                                 UpdateDate = v.UpdateDate
                                             }).ToList();
                return Ok(variants);
            }
            catch (Exception ex)
            {
                // Handle exceptions and return appropriate response
                return StatusCode(StatusCodes.Status500InternalServerError, $"An error occurred: {ex.Message}");
            }
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(TblVariant))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetVariantById(int id)
        {
            try
            {
                if(id == 0)
                {
                    return BadRequest();
                }

                // Find variant based on id and isDelete = false
                TblVariant variant = db.TblVariants.FirstOrDefault(x => x.Id == id && x.IsDelete == false);

                // Check if variant is null
                if (variant == null)
                {
                    return NotFound("Variant not found");
                }

                // Return variant if found
                return Ok(variant);
            }
            catch (Exception ex)
            {
                // Handle exceptions and return appropriate response
                return StatusCode(StatusCodes.Status500InternalServerError, $"An error occurred: {ex.Message}");
            }
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(TblVariant))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult CreateVariant([FromBody] TblVariant variant)
        {
            try
            {
                // Validate input if variant = null
                if (variant == null)
                {
                    return BadRequest("Invalid data variant");
                }

                // Check duplicate Name Variant and idCategory
                if (db.TblVariants.Any(x => x.IdCategory == variant.IdCategory && x.NameVariant.ToLower() == variant.NameVariant.ToLower() && x.IsDelete == false))
                {
                    ModelState.AddModelError("customError", $"Variant name : {variant.NameVariant} with category : {variant.IdCategory} already exist");
                    return BadRequest(ModelState);
                }

                // Initialize additional properties before adding to the database
                variant.CreateDate = DateTime.Now;
                variant.CreateBy = 1;
                variant.IsDelete = false;

                // Add new variant to the database
                db.TblVariants.Add(variant);
                db.SaveChanges();

                // Return respons HTTP 201 Created
                return CreatedAtAction(nameof(GetVariantById), new { id = variant.Id }, variant);
            }
            catch (Exception ex)
            {
                // Handle exceptions and return appropriate response
                return StatusCode(StatusCodes.Status500InternalServerError, $"An error occurred: {ex.Message}");
            }
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<TblVariant> UpdateVariant(int id, TblVariant data)
        {
            try
            {
                // Check if the updated variant data is valid
                if (data == null || id != data.Id)
                {
                    return BadRequest("Invalid variant data");
                }

                // Find the existing variant in the database
                TblVariant variant = db.TblVariants.Find(id);

                // Check if the variant exists
                if (variant == null)
                {
                    return NotFound("Variant not found");
                }

                // Validate for duplicate variants based on name and category
                // Check if there is another variant with the same name and category
                if (db.TblVariants.Any(x => x.Id != id && 
                        ((x.IdCategory == data.IdCategory && x.NameVariant.ToLower() == data.NameVariant.ToLower()) || 
                         (x.IdCategory != data.IdCategory && x.NameVariant.ToLower() == data.NameVariant.ToLower())
                        ) && x.IsDelete == false)
                   )
                {
                    // Get the category name based on data.IdCategory
                    string categoryName = db.TblCategories
                        .Where(c => c.Id == data.IdCategory)
                        .Select(c => c.NameCategory)
                        .FirstOrDefault();

                    ModelState.AddModelError(nameof(TblVariant.NameVariant), $"The variant name '{data.NameVariant}' within the category '{categoryName}' already exists. Please choose a different name or category.");
                    return BadRequest(ModelState);
                }

                // Update the existing variant properties
                variant.IdCategory = data.IdCategory;
                variant.NameVariant = data.NameVariant;
                variant.Description = data.Description;
                variant.UpdateBy = 1;
                variant.UpdateDate = DateTime.Now;

                // Save changes to the database
                db.SaveChanges();

                // Return success message
                return Ok("Variant updated successfully");
            }
            catch (Exception ex)
            {
                // Handle exceptions and return appropriate response
                return StatusCode(StatusCodes.Status500InternalServerError, $"An error occurred: {ex.Message}");
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult DeleteVariant(int id)
        {
            try
            {
                // Find the variant will be delete
                TblVariant variant = db.TblVariants.Find(id);

                if(id == 0)
                {
                    return BadRequest();
                }

                // Check if the category exists
                if (variant == null)
                {
                    return NotFound("Variant not found");
                }

                // Set value IsDelete to be true
                variant.IsDelete = true;

                // Save changes to the database
                db.SaveChanges();

                // Return success message
                return Ok($"Variant {variant.NameVariant} deleted successfully");
            }
            catch (Exception ex)
            {
                // Handle exceptions and return appropriate response
                return StatusCode(StatusCodes.Status500InternalServerError, $"An error occurred: {ex.Message}");
            }
        }

    }
}
