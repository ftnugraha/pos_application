CREATE TABLE [dbo].[TblCategory](
    [Id] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
    [NameCategory] [varchar](50) NOT NULL,
    [Description] [varchar](max) NULL,
    [IsDelete] [bit] NULL,
    [CreateBy] [int] NOT NULL,
    [CreateDate] [datetime2](7) NOT NULL,
    [UpdateBy] [int] NULL,
    [UpdateDate] [datetime2](7) NULL
	);

CREATE TABLE [dbo].[TblVariant](
    [Id] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
    [IdCategory] [int] NOT NULL,
    [NameVariant] [varchar](50) NOT NULL,
    [Description] [varchar](50) NULL,
    [IsDelete] [bit] NULL,
    [CreateBy] [int] NOT NULL,
    [CreateDate] [datetime2](7) NOT NULL,
    [UpdateBy] [int] NULL,
    [UpdateDate] [datetime2](7) NULL
)

CREATE TABLE [dbo].[TblProduct](
    [Id] [int] IDENTITY(1,1) Primary Key NOT NULL,
    [NameProduct] [varchar](100) NOT NULL,
    [Price] [decimal](18, 0) NOT NULL,
    [Stock] [int] NOT NULL,
    [IdVariant] [int] NOT NULL,
    [Image] [varchar](max) NULL,
    [IsDelete] [bit] NULL,
    [CreateBy] [int] NOT NULL,
    [CreateDate] [datetime2] NOT NULL,
    [UpdateBy] [int] NULL,
    [UpdateDate] [datetime2] NULL)

select*from TblCategory;
select*from TblVariant;
select*from TblProduct;

-- Insert dummy data into TblCategory
INSERT INTO TblCategory (NameCategory, Description, IsDelete, CreateBy, CreateDate, UpdateBy, UpdateDate)
VALUES 
('Clothing', 'Various clothing items', 0, 1, GETDATE(), NULL, NULL),
('Electronics', 'Electronic gadgets and devices', 0, 1, GETDATE(), NULL, NULL),
('Footwear', 'Different types of shoes', 0, 1, GETDATE(), NULL, NULL),
('Accessories', 'Fashion accessories', 0, 1, GETDATE(), NULL, NULL),
('Home and Furniture', 'Furniture and home decor', 0, 1, GETDATE(), NULL, NULL);

-- Insert dummy data into TblVariant
INSERT INTO TblVariant (IdCategory, NameVariant, Description, IsDelete, CreateBy, CreateDate, UpdateBy, UpdateDate)
VALUES 
(1, 'T-Shirt Red', 'Comfortable red T-Shirt', 0, 1, GETDATE(), NULL, NULL),
(1, 'Jeans Blue', 'Classic blue jeans', 0, 1, GETDATE(), NULL, NULL),
(2, 'Smartphone XYZ', 'High-performance smartphone', 0, 1, GETDATE(), NULL, NULL),
(3, 'Running Shoes', 'Lightweight running shoes', 0, 1, GETDATE(), NULL, NULL),
(4, 'Sunglasses', 'Stylish sunglasses', 0, 1, GETDATE(), NULL, NULL),
(5, 'Coffee Table', 'Modern coffee table', 0, 1, GETDATE(), NULL, NULL);

-- Insert dummy data into TblProduct
INSERT INTO TblProduct (NameProduct, Price, Stock, IdVariant, Image, IsDelete, CreateBy, CreateDate, UpdateBy, UpdateDate)
VALUES 
('Red T-Shirt Product', 29.99, 50, 1, 'red_tshirt.jpg', 0, 1, GETDATE(), NULL, NULL),
('Blue Jeans Product', 49.99, 30, 2, 'blue_jeans.jpg', 0, 1, GETDATE(), NULL, NULL),
('Smartphone XYZ Product', 499.99, 20, 3, 'smartphone_xyz.jpg', 0, 1, GETDATE(), NULL, NULL),
('Running Shoes Product', 79.99, 40, 4, 'running_shoes.jpg', 0, 1, GETDATE(), NULL, NULL),
('Sunglasses Product', 29.99, 60, 5, 'sunglasses.jpg', 0, 1, GETDATE(), NULL, NULL),
('Coffee Table Product', 129.99, 10, 6, 'coffee_table.jpg', 0, 1, GETDATE(), NULL, NULL);

truncate table TblVariant;
drop table TblProduct;



