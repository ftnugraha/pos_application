﻿namespace Pos_ViewModel
{
    public class VM_Variant
    {
        public int Id { get; set; }

        public int IdCategory { get; set; }

        public string NameVariant { get; set; } = null!;

        //Add NameCategory
        public string NameCategory { get; set; }

        public string? Description { get; set; }

        public bool? IsDelete { get; set; }

        public int CreateBy { get; set; }

        public DateTime CreateDate { get; set; }

        public int? UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }
    }
}
