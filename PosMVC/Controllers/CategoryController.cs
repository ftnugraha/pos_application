﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PosApi.Models;
using System.Text;

namespace PosMVC.Controllers
{
    public class CategoryController : Controller
    {
        //base url
        Uri baseAddress = new Uri("http://localhost:5206/api");

        private readonly HttpClient _client;

        //constructor
        public CategoryController()
        {
            _client = new HttpClient();
            _client.BaseAddress = baseAddress;
        }

        [HttpGet]
        public IActionResult Index()
        {
            List<TblCategory> listCategory = new List<TblCategory>();
            HttpResponseMessage response = _client.GetAsync(_client.BaseAddress + "/CategoryApi").Result;

            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                //mengubah format json menjadi objek
                listCategory = JsonConvert.DeserializeObject<List<TblCategory>>(data);
            }

            return View(listCategory);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(TblCategory model)
        {
            try
            {
                //ubah data objek ke format json 
                string data = JsonConvert.SerializeObject(model);
                StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                HttpResponseMessage response = _client.PostAsync(_client.BaseAddress + "/CategoryApi", content).Result;

                if (response.IsSuccessStatusCode)
                {
                    TempData["successMessage"] = "Category created.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                TempData["errorMessage"] = ex.Message;
                return View();
            }

            return View();
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            try
            {
                TblCategory category = new TblCategory();
                HttpResponseMessage response = _client.GetAsync(_client.BaseAddress + "/CategoryApi/" + id).Result;

                if (response.IsSuccessStatusCode)
                {
                    string data = response.Content.ReadAsStringAsync().Result;
                    category = JsonConvert.DeserializeObject<TblCategory>(data);
                }

                return View(category);
            }
            catch (Exception ex)
            {
                TempData["errorMessage"] = ex.Message;
                return View();
            }
        }

        [HttpPost]
        public IActionResult Edit(TblCategory model)
        {
            try
            {
                //ubah format objek ke json
                string data = JsonConvert.SerializeObject(model);
                StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                var url = _client.BaseAddress + "/CategoryApi/" + model.Id;
                HttpResponseMessage response = _client.PutAsync(url, content).Result;

                if (response.IsSuccessStatusCode)
                {
                    TempData["successMessage"] = "Todo updated.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                TempData["errorMessage"] = ex.Message;
                return View();
            }

            return View();
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            try
            {
                TblCategory category = new TblCategory();
                HttpResponseMessage response = _client.GetAsync(_client.BaseAddress + "/CategoryApi/" + id).Result;

                if (response.IsSuccessStatusCode)
                {
                    string data = response.Content.ReadAsStringAsync().Result;
                    category = JsonConvert.DeserializeObject<TblCategory>(data);
                }

                return View(category);
            }
            catch (Exception ex)
            {
                TempData["errorMessage"] = ex.Message;
                return View();
            }
        }

        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            try
            {
                HttpResponseMessage response = _client.DeleteAsync(_client.BaseAddress + "/CategoryApi/" + id).Result;

                if (response.IsSuccessStatusCode)
                {
                    TempData["successMessage"] = "Todo deleted.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                TempData["errorMessage"] = ex.Message;
                return View();
            }

            return View();
        }
    }
}
